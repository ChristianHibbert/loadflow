# -*- coding: utf-8 -*-
"""
Created on Tue Jul 17 12:08:42 2018

@author: christian.hibbert
"""

import csv
from contextlib import contextmanager

from test import Dict

# data will need to be output to files or dealt with for control

# 1. Config file - get the name and location of the files to create 
# 2. Holder (dictionaries?) for the data movement 
# 3. 

# Data Array for various performance parameters?
#   collected from CollectNetStatus
#   is file open? - method to do this and one to close...
#   


class ResultsFile:
    def __init__(self):
#        with open('Pall.csv', 'a') as f, open('Vall'+'.csv', 'a') as g, open('Qall'+'.csv', 'a') as h:
#    
#            self.writer1 = csv.writer(f)
#            self.writer2 = csv.writer(g)
#            self.writer3 = csv.writer(h)
#
#        self.aa = (self.writer1,self.writer2,self.writer3)
        
        self.writer1 = open('Vall'+'.csv', "w")     # we can loop this with details laid out in the config file
        #self.writer1 = open(filename, "w")     # we can loop this with details laid out in the config file

        

    def closeFile(self):
        
        """
        close all files 
        """
        
        self.writer1.close()
        print('check closure')
        
        
class writeResults():
    
    def __init__(self):
        self.Files = ResultsFile()
        self.Results = Dict()
        #return Busloads
        #self.PowerDict() # if we'll use this within successive functions within the class we'll
    
#    @contextmanager    
#    def context(self):
#        self.Files = 
#        yield self
#        self.Files.closeFile()

        
    def resultsContainer(self,a): # a will be more of lists within the dictionary
        
        x = ('volts', 'a' ,a)
        print(self.Results.update_dict2(x))
        

    def writeRow(self,a):
        #self.test.writer1.writerow(a)
        print(self.Results.myDict)
        #self.test.writer1.write(self.Results.myDict['volts']['a'])
        self.Files.writer1.write(str(a)[1 : -1])
        #csv.write(row)
        
    def closeFiles(self):
        self.Files.closeFile()
        #test = ResultsFile()


#with writeResults().context() as test:
#    for 
#        test.writeRow(range(10))





        
a = range(10)        
test = writeResults()
#test.resultsContainer(a)
test.writeRow(a)
#test.writer1.write(a)
#aaa = ResultsFile()

#This will be placed at the end of all simulations
test.closeFiles()

#writer1 = open('Vall'+'.csv', "w") 
#writer1.write(str(a)[1 : -1])
#writer1.close()






#
#
#
#
#
#
#    writer4 = csv.writer(z)
#    writer5 = csv.writer(zz)
#    
#    writer1.writerow(OrderedBus)
#    writer2.writerow(OrderedBus)
#    writer3.writerow(OrderedBus)
#    writer4.writerow(['MW_F1','MW_F2','MW_F3','MW_F4','MW_F5','MW_F6','MW_F7','MW_F8', 
#                      'MVAr_F1','MVAr_F2','MVAr_F3','MVAr_F4','MVAr_F5','MVAr_F6','MVAr_F7','MVAr_F8']) 
#    writer5.writerow(OrderedBus)
#    
#    
#    
#    # xlSheet method - look into this
#            writer4.writerow([MWlosses['Feeder1'],MWlosses['Feeder2'],MWlosses['Feeder3'],MWlosses['Feeder4'],
#                              MWlosses['Feeder5'],MWlosses['Feeder6'],MWlosses['Feeder7'],MWlosses['Feeder8'],
#                              MVArLosses['Feeder1'],MVArLosses['Feeder2'],MVArLosses['Feeder3'],MVArLosses['Feeder4'],
#                              MVArLosses['Feeder5'],MVArLosses['Feeder6'],MVArLosses['Feeder7'],MVArLosses['Feeder8']])
#            
#            for itemS in OrderedBus:
#                Varr.append(Vdict[itemS])    
#            writer3.writerow(Varr)
#            
            
            
            #####################################################################################################################
            
#class WriteResults(DataMovement):
#    """
#    Methods that add headings,results and footer to an output file
#    """
#    def __init__(self, config):
#        self.dictResult = {}
#        super(WriteResults, self).__init__(config)
#        self.config = config
#        self.sourceFolder = self.rootFolder + config.get('paths', 'output')
###        print "this is source folder", self.sourceFolder
#        self.fileName = self.sourceFolder + config.get('files', 'output')
###        print "this is file name", self.fileName
#        #determine from config file which results are supposed to be written to csv
#        self.resultList = []
#        for name in range(len(config.options('results1'))):
#            self.resultList.append(config.get('results1', str(int(name) + 1)))
#        for name in config.options('voltages'):
#            self.resultList.append('v_' + name)
#        for name in config.options('assets'):
#            self.resultList.append('ass_' + name)
#        for name in range(len(config.options('results2'))):
#            self.resultList.append(config.get('results2', str(int(name) + 1)))
#        event = 0
#        for id in config.sections():
#            if id[:5] == 'event':
#                self.resultList.append('oolEvent' + str(event))
#                event = event + 1
#
###        print "this is result list", self.resultList
#
###    def writeHeaders(self):
###        # sub headings
###        for num in range(len(self.resultList)):
###            print "testing num", num
###            # major headings
###            majHeadings = []
###            for name in range(48):
###                majHeadings.append(name)
#####            print "majheadings", majHeadings
###            for post in range(len(self.config.options('postoutputs'))):
###                with open( self.config.get('postoutputs', str(int(post) + 1))+'.csv', 'w') as fOutput:
###                    opWriter = csv.writer(fOutput, lineterminator='\n')
###                    opWriter.writerow(majHeadings)
###                    fOutput.close()
###        return True
#    def writeHeaders(self):
#        # sub headings
#        subHeadings = []
#        for name in range(len(self.config.options('subhead1'))):
#            subHeadings.append(self.config.get('subhead1', str(int(name) + 1)))
#        nr_v = -1
#        for name in self.config.options('voltages'):
#            subHeadings.append(name)
#            nr_v = nr_v + 1
#        nr_ass = -1
#        for name in self.config.options('assets'):
#            subHeadings.append(name)
#            nr_ass = nr_ass + 1
#        for name in range(len(self.config.options('subhead2'))):
#            subHeadings.append(self.config.get('subhead2', str(int(name) + 1)))
#        print "testing subHeadings", subHeadings
#        # major headings
#        majHeadings = []
#        for name in range(len(self.config.options('majhead1'))):
#            majHeadings.append(self.config.get('majhead1', str(int(name) + 1)))
#        majHeadings = majHeadings + ['Voltages %'] + [''] * nr_v + \
#            ['Asset utilisation %'] + [''] * nr_ass
#        print "testing maj headings1", majHeadings
#        for name in range(len(self.config.options('majhead2'))):
#            majHeadings.append(self.config.get('majhead2', str(int(name) + 1)))
#        print "testing maj headings2", majHeadings
#        fOutput = open(self.fileName, 'w')
#        opWriter = csv.writer(fOutput, lineterminator='\n')
#        opWriter.writerow(majHeadings)
#        opWriter.writerow(subHeadings)
#        fOutput.close()
#        return True
###    def addattr(self,x,val):
###        self.__dict__[x]=val
###
###    self.addattr('bb' + str(bus), self.i_pen)#problem here
#
#    def writeResultRow(self, resultDict): #, mainCount): - use this with a dictionary later
#        """
#        Writes the results in resultDict to output file.
#        """
#        # write results into csv file
#        for datum in self.resultList:
#            print "this is datum",datum
#            self.dictResult.setdefault(datum,[]).append(resultDict[datum])
#        print"testing dictresult", self.dictResult
###        print yyy
#        return True
#
#    def writeResultFinal(self):
###        print"testing dictresult", self.dictResult
###        print stop
#        for k,v in self.dictResult.iteritems():
#            if k == "timeStamp":
#                pass
#            else:
#                for post in range(len(self.config.options('postoutputs'))):
#
#                    if self.config.get('postoutputs', str(int(post) + 1)) == k:
#
#                        #v_busbar2
#
#                        #NUM = (mainCount*100) + bus
#                        #fOutput.write(str(NUM) + ",") # this writes the implicit busbar number in column 1...file saver
###                        print "testing", v
###                        stop
#                        with open( k +'.csv', 'a') as fOutput:
#                            fOutput.write(str(v) + ",")
#        ##                            opWriter = csv.writer(fOutput, lineterminator='\n')
#        ##                            opWriter.writerow(resultDict[datum])
#                            fOutput.write('\n')
#                            fOutput.close()
###        print yy
###        fOutput = open(self.fileName, 'a')
###        opWriter = csv.writer(fOutput, lineterminator='\n')
###        resultRow = []
###        for datum in self.resultList:
###            print "this is datum",datum
###            resultRow = resultRow[:] + [resultDict[datum]]
###            print "this is result",[resultDict[datum]]
###            print "this is", resultRow
###        print yyyy
###        opWriter.writerow(resultRow)
###        fOutput.close()
#        return True
#
#
#
#        #HDF5
#        #self._HDF5['Pre'][cust]['BUS'+str(bus)]['time'][1,t]
###        self.iter_ = main.loop_count
###
###        for key in resultDict.iteritems():
###            if key == 'timestamp':
###                t_index = timestamplist.index(resultDict[keyey])
###
###            elif key[0:8] != 'v_busbar':
###                self.HDF5['Post'][key][self.iter_,t_index]
###
###            elif key[0:8] == 'v_busbar':
###                self.HDF5['Post']['v_bus'][keyey[2:len(key)]][self.iter_,t_index]
#
#
#    def writeRows(self, lines):
#        """
#        Writes rows of data into the output file.
#        This is for when data is missen. It writes only the timestamp
#        """
#        fOutput = open(self.fileName, 'a')
#        opWriter = csv.writer(fOutput, lineterminator='\n')
#        opWriter.writerows(lines)
#        fOutput.close()
#        return True