# -*- coding: utf-8 -*-
"""
Created on Wed Jul 04 16:05:23 2018



@author: christian.hibbert
"""

__metaclass__ = type

##import numpy as np
import csv,os
#from TEST11 import FeederCount
#from TEST11 import RunMC
import datetime
from test import Dict


class DataMovement:
    """
    Base class for any process requiring interaction with a file.
    """
    def __init__(self, config):
#!        self.projPath  = config.get('paths', 'project')
        self.projPath = self.recursiveDirLookup(3)
##        print "this is project path", self.projPath
        print(self.projPath)
        self.rootFolder = self.projPath + config.get('paths', 'root')
        print "this is root folder", self.rootFolder
    def recursiveDirLookup(self, depth):
        cwd = os.getcwd()
        for i in range(depth):
            dir = os.path.split(cwd)[0]
            if cwd != dir:
                cwd = dir
            else:
                return(dir)
        return(dir)

        
        
class LoadCapacity:
    def __init__(self, config, mainCount):
        self.LoadsFile = csv.DictReader(open('SubLoadskVA.csv')) # [config]
        self.config = config
        #self.periods = self.config.get('simulation', 'startPeriod')
        #self.periods = ('00:00','01:00') # this must come from config file
        #[datetime.strptime(x, '%M:%H') for x in periods]
        self.start = self.config.get('simulation', 'startPeriod')
        self.end = self.config.get('simulation', 'endPeriod')
        
        self.start_time = datetime.datetime.strptime(self.start, '%H:%M')
        self.end_time = datetime.datetime.strptime(self.end, '%H:%M')
        
        
        
#        self.start_time = datetime.datetime.strptime(self.periods[0], '%H:%M')
#        self.end_time = datetime.datetime.strptime(self.periods[1], '%H:%M')
        
        self.mins = datetime.timedelta(minutes=30) # minutes must come from config file
        self.BusLoads = Dict()
        self.PowerDict() 
        
    
    def simPeriods(self):

        """get the periods for simulation"""
        
        now = self.start_time
        Periods = []
        fmt = "%H:%M"
        while now <= self.end_time:
            #print(now.strftime(fmt))
            a = str(now.strftime(fmt))
            Periods.append(a)
            now += self.mins
        
        print Periods
        return Periods
    
    
    def PowerDict(self):
        
        #BusLoads = Dict()
        #self.BusLoads = Dict()
        
        # data frames will be better here....Pointless looping like this (ok for a couple of input columns though)
        # Alternatively change the data input file structure - rows not columns.. 
        
#        now = self.start_time
#        Periods = []
#        fmt = "%H:%M"
#        while now <= self.end_time:
#            #print(now.strftime(fmt))
#            a = str(now.strftime(fmt))
#            Periods.append(a)
#            now += self.mins
#        print Periods
        #xxxx
        
        Periods = self.simPeriods()
        
        for row in self.LoadsFile:
            
            if row['Substation'] == '':
                break;
            else:
                for period in Periods:
                    #self.BusLoads.add_2_attr(period, row['Substation'],row[period])
                    
                    #self.BusLoads.update_dict({(period, row['Substation']):row[period]})
                    x = (period, row['Substation'],row[period])
                    self.BusLoads.update_dict2(x)
                    
            x = ('BaseCase', row['Substation'],row['BaseCase'])
            self.BusLoads.update_dict2(x)
                    #BusLoads.update_dict({(period, row['Substation']):row[period]})
        #return BusLoads 
        
        
        
          
                        
    def PowerList(self,t):
        
        """ 
            List of elements corresponding to bus names and loads
        
        """
        
        BusIDs = []
        LoadMag = []
        self.LoadMag = Dict()
        print(self.BusLoads.myDict)
        print t
        
        
        #for time in time: #timeinstance - what time are we on?
        #for k, v in self.BusLoads.myDict[t].iteritems()
        source_list = list([i for i in list(self.BusLoads.myDict[t].iteritems())])
        list1, list2 = zip(*source_list)
        #source_list = list([i for i in list(a.myDict['cat'].iteritems())])
        #unpack tuples in list - zip(*source_list)
        print ('this is list 1', list1)
        print ('this is list 2', list2)
        #xxxxxx
        powerKeys = dict(zip(list1, list2))
        return powerKeys

#        for key in self.BusLoads.myDict:
#            print(key)
#            self.BusLoads.update_dict({(period, row['Substation']):row[period]})     
#        # or use the zip function to decouple elements
#        times = list(set([i[0] for i in list(self.BusLoads.myDict.keys())]))
#        a = set(times).intersection(self.periods)


class IPSAModel(DataMovement):
    """
    Method to get the network path and location
    """
    def __init__(self, config):
        self.config = config
        super(IPSAModel, self).__init__(config)
        self.sourceFolder = self.rootFolder + config.get('paths', 'model')
        
        
##        print "this is sourcefolder", self.sourceFolder
#    def getNewFileLoc(self):
#        fileName = self.sourceFolder + 'Moorside_v1_TJ_Copy'
###        print "filename1", fileName
#        return fileName
    def getFileLoc(self):
        fileName = self.sourceFolder + self.config.get('files', 'model')
##        print "filename2", fileName
        return fileName


