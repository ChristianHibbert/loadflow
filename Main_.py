# -*- coding: utf-8 -*-
"""
Created on Wed Jul 04 15:52:55 2018

Purpose:     Main load flow script

@author: christian.hibbert
"""

__metaclass__ = type

from utilities import TimeRun
from loadflowcontrol_ import *
##from loadflowcontrol_tapess import *
##from Tap_ESSloadflowcontrol import *
from ConfigParser import ConfigParser
from optparse import OptionParser
import sys
import gc
import os
import datetime
#import TEST_HDF5
from DataFiles3_ import ResultsFile

def __main__():
    print "test main"
    # make stderr and stdout go to files
##    sys.stderr = open('errlog.txt', 'w')
##    sys.stdout = open('outlog.txt', 'w')

    # set the garbage collector parameters
    gc.enable()
    gc.set_debug(gc.DEBUG_LEAK)
    gc.set_debug(gc.DEBUG_STATS)
    gc.set_debug(gc.DEBUG_UNCOLLECTABLE)


    projPath  = 'C:\\Users\\christian.hibbert\\Desktop\\Network Modelling\\'
    rootFolder = projPath + 'Bridgend - 560024\\'
    fileName = rootFolder + 'Configurations\\config2.txt' #

    
    # read configuration file supplied on the command line
    config = ConfigParser()

    config.read(fileName)
    
    # start log
    logObj = TimeRun()
    if not logObj.startLog(): raise Exception('Log failed to start')
    busids = ['Bus' + str(i + 1) for i in xrange(33)]
    d_results = {k: [] for k in busids}

    results = ResultsFile(config)

    mainCount = 1
    while mainCount < 2:
        print"run1"
        run = loadFlowControl(config, mainCount, results) #
        print('Run',  run)
        #xxxxx
        print"run2"
        if not run.processWeeks():
            raise Exception('main loop failed')
        print"running simulations 5 times"
        mainCount +=1

    # append footer with log info
#    if not logObj.endLog(config): raise Exception('Log failed to close')
    results.closeFile()
    print ('All done!!!!')
# Indication the code has finished....

if __name__ == '__main__':
    print(os.getcwd())
    #xxx
    __main__()

