# -*- coding: utf-8 -*-
"""
Created on Wed Jul 04 15:47:57 2018

# Name:       loadflowcontrol
# Purpose:    Controls the all aspects of the simulation

@author: christian.hibbert
"""

from __future__ import division
__metaclass__ = type
#from storagedevice import SubstationESS
#from storagedevice import NetworkESS
#from networkobservation import *
#from controlactions import *
from datamovement import *
from networksimulation_ import *
##from ipsa import IscInterface
##from ipsa import IscTransformer
import ipsa
import datetime
from test import Dict
from DataFiles3_ import * #ResultsFile #writeResults





class loadFlowControl(LoadFlowModel):
    """
    Coordinates objects to provide annual simulation
    """
    def __init__(self, config, mainCount, results):
        """
        Main loop initialisation.
        """
        #print 'this is mainCount', mainCount
        self.mainCount = mainCount
        print('Enetered loadFlowControl')
        self.config = config # create an instance of the config parser
        self.datasource = LoadCapacity(config, mainCount) # 
        
        print(self.datasource.BusLoads)
        
        self.PF = config.getfloat('loads', 'PF') #0.98                              # get from config file
        self.weeks = config.getint('simulation', 'weeks')
        super(loadFlowControl, self).__init__()
#        super(CollectNetStatus, self).__init__()
        self.scaleFactglobal = self.config.getfloat('loads', 'scaleFact') #1.5 # [config] self.config
        self.results = results
        self.control = ControlAction(self.config, self.net, self.ipsasys) # get handle to control unit...
        
    
    def processWeeks(self): #processPeriods/seasons/ ...Call this 'processPeriods(self):
        """
        Here we set up the simulation horizon obtaining the demands on each busbar across the network 
        """
        
        # Before anything - get the base demands and send to baseCase()
        self.baseCase()
        self.baseV = self.nomV
        self.baseTap = self.tapPC 
        
        
        periods = (self.config.get('simulation', 'periods'))
        periods = periods.translate(None, "()''")           #periods = re.sub('[()]', '', periods)
        #datetime.datetime.strptime(a[0], '%H:%M')
        periods = periods.split(',')
        
        
        
        simWindow = LoadCapacity(self.config, self.mainCount)
        Periods = simWindow.simPeriods()
        
        
        for x in Periods:
            self.scaleFact = self.scaleFactglobal
            
        
            
        
            #t = x #periods[0] # we're only doing one period...
            self.t = x
            self.powerKeys = self.datasource.PowerList(self.t) # instance of the demand at a given time - dictionary or list?
        
#        with LoadFLowModel(self.config) as simulation:
        #self.insertValues(self.powerKeys,self.scaleFact) # not for just demand but transformer positions etc
                
            exitcode = self._processRow()
            
        return 1
        
        

    def _processRow(self):
        """

        """
#        self.ipsasys, self.net = simulation.getNetRef() # Makes the handle from the simulation available outside this class -- self.ipsasys, self.net in 'networksimulation_py.
        exitcode = 1;
#        while exitcode < 1:
        print ('This gets the time and loads', self.powerKeys)
        #Ensure...
        self.nomV = self.baseV
        self.Tap = self.baseTap
        
        
        while exitcode != 0:
            print exitcode
            self.insertValues(self.powerKeys,self.scaleFact) # not for just demand but transformer positions etc
            
            if not self.doLoadFlow(): raise Exception('Load flow failed') # calls doLoadFlow method - self.net.DoLoadFlow()
            
            # what is the status of the system...SEVERAL PERFORMANCE PARAMETERS
    
            self.Logs = CollectNetStatus(self.results, self.net, self.config) 
            #resultlog1, NomVolts  = resultlog.getNetParams() # Is it worth passing nominal volts here..., maybe         
            resultlog, vLimits  = self.Logs.getNetParams(self.t) # send the parameters for busbars here....
            self.resultlog = resultlog
            print resultlog
            #self.Logs = CollectNetStatus(self.results, self.net, self.config) 
            
            if (self.resultlog['underV'] == 0 and self.resultlog['overV'] ==0):
                exitcode = 0
                print resultlog
                #xxxxx
            
            
            
            #control = ControlAction(self.config, self.net, self.ipsasys) # get handle to control unit...
            #if resultlog['total'] > 0 or any([len(resultlog['underV']) ==0, len(resultlog['overV'])==0]):
            if any([self.resultlog['underV'] >=0, self.resultlog['overV']>=0]):
                    
                if resultlog['underV'] > 0:
                    exitcode = self.underTapPC(self.t)# if voltage cannot be resolved then resort back to the 
                    self.nomV = self.nomV
                    self.Tap = self.tapPC     
                    
                elif resultlog['overV'] > 0:
                    exitcode = self.overTapPC(self.t)# if voltage cannot be resolved then resort back to the
                    self.nomV = self.nomV
                    self.Tap = self.tapPC 
            
            
            
            if exitcode == -1: # get the difference in demand from baseCase, then perform DSR
                self.ApparentPowerLoads = self.getLoads()
                print self.baseDemand
                print self.ApparentPowerLoads
    
                DSRdict = {}
                for key, val in self.ApparentPowerLoads.iteritems():
                    if (float(val) - float(self.ApparentPowerLoads[key])) < 0: # keep Apparent loads as is
                        DSRdict[key] = float(val)
                    else:
                        DSRdict[key] = float(val) - float(self.baseDemand[key])
                        #print val, self.baseDemand[key], (float(val) - float(self.baseDemand[key]))
                        #xxx
                print DSRdict # send this to 'controlAction' for DSR enactment 
                
                # DSRcall = DSRoptions(DSRdict)
                
                
                print (self.baseTap, self.baseV,self.Tap, self.nomV)
                
                self.powerKeys = self.baseDemand
                self.scaleFact = 0
                
                # for now we'll revert back to the baseCase voltage level
                
                 
                self.control.basetapsetting(self.baseV)
                
                
        print exitcode        
            #self.insertValues(self.powerKeys,0)
            #if not self.doLoadFlow(): raise Exception('Load flow failed') # calls doLoadFlow method - self.net.DoLoadFlow()
            #self.Logs = CollectNetStatus(self.results, self.net, self.config)  
            #resultlog, vLimits  = self.Logs.getNetParams() # send the parameters for busbars here...
            #self.resultlog = resultlog
        
        #control = ControlAction(self.config, self.net, self.ipsasys) # get handle to control unit...
        #if resultlog['total'] > 0 or any([len(resultlog['underV']) ==0, len(resultlog['overV'])==0]):
        if any([self.resultlog['underV'] >=0, self.resultlog['overV']>=0]):
            print self.resultlog
            #xxxxx
        
        """ send to control actions for transformers """
            #overV or underV situation?
        #resultlog, nomV = tapControlOperations()
        #self.resultlog = resultlog
        print self.resultlog
        print (self.baseTap, self.baseV,self.Tap, self.nomV)                
                    
        
    def controlSequence():
        """ figure control order to solve network problems - calls controlAction to determine limits"""
        
        # series of case statements here
        
    # adjust tap postion, enact DSR, source or sink real power to an energy storage device 
    
    # link with other actors in the system to obtain possible constraints 
        # achieved by a matrix of options
        # Assume we come back from here and altering the tap changer is the answer
        
        # ControlAction() # don't come out of this control unless solved - once all excursions soved then write to file
    
        # self.controlSeq = [] # could be a dictionary - what would be needed to get the network back to limits
    
    
    def DSRoptions(): # Not necessarily required - depends on future modelling arcitecture for EPO
        
        """ method that links with other actors to determine feasible bounds """
    
        
        # 
        # call to ESP
        # send/call to DSR
        
    def tapControlOperations(self):
        """ revert to old code if needed """
        
        
#        if any(value == 0 for value in self.resultlog.values()) == True: # suggests and under or over voltage issue...
#            if self.resultlog['underV'] > 0:
    
    def underTapPC(self,t):
        
        #print(self.resultlog['underV'])
        Vcond = self.resultlog['underV']
        for V in xrange(100,107):
            print ('Entered underV loop', V/100)
            tapPC, NominalV = self.control.tapsetting(V/100)
            self.tapPC = tapPC; self.nomV = NominalV
            if not self.doLoadFlow(): raise Exception('Load flow failed')
            resultlog, vLimits  = self.Logs.getNetParams(t) # send the parameters for busbars here.... 
            self.resultlog = resultlog
            if self.resultlog['underV'] ==0:
                exitcode = 0
                print ('no more under volts')
                return exitcode
                #break;
        if self.resultlog['underV'] != 0: #> Vcond:
            print ('its getting worse')
            exitflag = -1
            #xxx
            return exitflag
                        #break;
                        
#            if self.resultlog['overV'] > 0:
    def overTapPC(self,t):
        
        Vcond = self.resultlog['overV']
        for V in xrange(94,101):
            print ('Entered overV loop')
            tapPC, NominalV = self.control.tapsetting(V/100)
            self.tapPC = tapPC; self.nomV = NominalV
            if not self.doLoadFlow(): raise Exception('Load flow failed')
            resultlog, vLimits  = self.Logs.getNetParams(t) # send the parameters for busbars here....  
            self.resultlog = resultlog
            if self.resultlog['overV'] ==0:
                exitcode = 0
                print ('no more over volts')
                return exitcode
#                print ('no more over volts')
#                break;
        if self.resultlog['underV'] != 0: #> Vcond:
            print ('its getting worse')
            exitflag = -1
            #xxxx
            return exitflag
        
#            elif self.resultlog['overV'] > Vcond:
#                print ('its getting worse')
#                break;

    
    def baseCase(self):
        """ Determine base voltage level """
        
        # get loads
        scaleFact = 0
        t = 'BaseCase'
        self.baseDemand = self.datasource.PowerList(t) # instance of the demand at a given time - dictionary or list?
        self.insertValues(self.baseDemand,scaleFact) # not for just demand but transformer positions etc
        #if any(value == 0 for value in self.resultlog.values()) == True:
        exitcode = 1
        i = 0
        while exitcode > 0:
            
            if i == 0:
                #self.control = ControlAction(self.config, self.net, self.ipsasys) # get handle to control unit...
                #Ensure starting positions are set at unity...Tap locked at zero position with unity power factor setting..
                nomV = 1
                tapPC, baseNominalV = self.control.basetapsetting(nomV)
                if not self.doLoadFlow(): raise Exception('Load flow failed')
                self.Logs = CollectNetStatus(self.results, self.net, self.config) 
                resultlog, vLimits  = self.Logs.getNetParams(t) # send the parameters for busbars here....            
                self.resultlog = resultlog
                if self.resultlog['total'] == 0:
                    exitcode = 0
                    
            elif resultlog['underV'] > 0:
                exitcode = self.underTapPC(t)# if voltage cannot be resolved then resort back to the 
                print ('entered Else statement for Base computation')
                #tapPC, baseNominalV = control.tapsetting(nomV) 
                #exitcode = self.tapControlOperations()
                self.baseV = self.nomV
                self.baseTap = self.tapPC     
                
            elif resultlog['overV'] > 0:
                exitcode = self.overTapPC(t)# if voltage cannot be resolved then resort back to the
                print ('entered Else statement for Base computation')
                #tapPC, baseNominalV = control.tapsetting(nomV) 
                #exitcode = self.tapControlOperations()
                self.baseV = self.nomV
                self.baseTap = self.tapPC 
            
            
            if exitcode == -1:
                print ("this is what you don't want to see ")
                xxxxxx
            
            #if flag exist, artificailly break the code - then set exitcode = 0 
            #exitcode = 0 
            i+=1
        print (i,tapPC,baseNominalV)