# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 15:18:36 2018

@author: christian.hibbert
"""
import ipsa
import math
from time import strptime
#from networksimulation import LoadFlowModel, CollectNetStatus
#class ControlAction(CollectNetStatus): #inherit from CollectNetStatus
class ControlAction: #inherit from CollectNetStatus
    """ Base class to control voltage events (initially) on the network """
    
    
    def __init__(self,config,net,ipsasys):
        """ get status of network """
        
        #self.Vlevel = 11 use config file to get the status of the network
        # get possible tap positions
        
        self.config = config
        self.net = net
        self.ipsasys = ipsasys
        
        self.Vlevel = self.config.getfloat('characteristics', 'nominalVoltagekV')
        self.txName = self.config.get('transformers', 't1')
        self.tx = self.net.GetTransformer(self.txName)


    def basetapsetting(self,nomV): # Real power (for now)
        """ Change tap position according to the voltage issues""" 
        
        self.tx.SetBValue(ipsa.IscTransformer.LockTap, False)
        self.tx.SetDValue(ipsa.IscTransformer.SpecVPU, nomV)
        tapPC = self.tx.GetDValue(ipsa.IscTransformer.TapStartPC)
        NominalV = self.tx.GetDValue(ipsa.IscTransformer.SpecVPU)
        return tapPC, NominalV
        
    def tapsetting(self,nomV):
        
        self.tx.SetBValue(ipsa.IscTransformer.LockTap, False)
        self.tx.SetDValue(ipsa.IscTransformer.SpecVPU, nomV)
        tapPC = self.tx.GetDValue(ipsa.IscTransformer.TapStartPC)
        NominalV = self.tx.GetDValue(ipsa.IscTransformer.SpecVPU)
        return tapPC, NominalV
        

#        
#        
#        #print "get tap start v1",self.tx.GetDValue(ipsa.IscTransformer.TapStartPC)
#        #print "set tap start v1",self.tx.SetDValue(ipsa.IscTransformer.TapStartPC, 4)
#        #print "get tap start v1",self.tx.GetDValue(ipsa.IscTransformer.TapStartPC)
#        #print(self.tx)
#        
      
    def DSR(self): 
        """ Enact DSR to turn down or turn up demand """
        
        #1. Get voltage issues
        #2. [*Issue call to ESP*]
        #3. Recive call back from DSR
        #4. Compute load flow response - this probably should be done prior to step 2 to understand the magnitude of load reduction
        
    def sourceESS(self): # pricing signals, undervoltage (real power), overvoltage (reactive power injection)
        """ Enact energy storage to source real and reactive power """
    
    def sinkESS(self): #pricing signals,.....
        """ Enact energy storage to sink real and reactive power """
        
        
        