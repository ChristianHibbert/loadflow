# -*- coding: utf-8 -*-
"""
Created on Tue Jul 17 12:08:42 2018

@author: christian.hibbert
"""

import csv
from contextlib import contextmanager
from test import Dict
#from networksimulation_w import *

class ResultsFile:

    def __init__(self, config):
        
        
        ####self.writer1 = open('Vall'+'.csv', "w")     # we can loop the config file to create a handle to all data files
        self.config_time = xrange(10) #config 
        self.config_headers = ['Bus' + str(i + 1) for i in xrange(33)]
        self.config_headers.insert(0,'time')
        
        self.d_writers = {}
        for t in self.config_time:
            writer = 'writ' + str(t)
            self.d_writers[writer] = open('V' + str(t) + '.csv', 'wb')
            self.writeHeaders(writer)
            # setattr(self, 'writ' + str(t), open('V' + str(t) + '.csv', 'w'))
        
#        setattr(self, 'writ1', open('V1'+'.csv', "w")) 
#        setattr(self, 'writ2', open('V2'+'.csv', "w"))
#        setattr(self, 'writ3', open('V3'+'.csv', "w")) 
#        self.writer1 = (getattr(self, 'writ1'),getattr(self, 'writ2'),getattr(self, 'writ3')) #use an append function
        
#        self.writeHeaders()
    
    def writeHeaders(self, writer):
        """ Switch statements to write headers in each file"""
        
#        self.Files.writer1[x].write(str(a)[1 : -1])
        self.d_writers[writer].write(', '.join(self.config_headers))
        self.d_writers[writer].write('\n')
    
    def writeRow(self, writer, li_row):
        
        self.d_writers[writer].write(', '.join(li_row))
        self.d_writers[writer].write('\n')
    
    def closeFile(self):
        
        """
        close all files 
        """
#        for x in range(len(self.writer1)):
#            self.writer1[x].close()
#            print(self.writer1[x])
        for v in self.d_writers.itervalues():
            v.close()