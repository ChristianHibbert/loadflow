# -*- coding: utf-8 -*-
"""
Created on Wed Jul 11 10:11:38 2018

@author: christian.hibbert
"""
#from sys import exit
# define Python user-defined exceptions
class Error(Exception):
   """Base class for other exceptions"""
   pass

class ValueTooSmallError(Error):
   """Raised when the input value is too small"""
   exitcode = 1
   pass 

class ValueTooLargeError(Error):
   """Raised when the input value is too large"""
   pass


exitcode = 0
while exitcode < 1:

    a = 1
    print a
    exitcode += 1
xxxx
# our main program
# user guesses a number until he/she gets it right

# you need to guess this number
number = 10
exitcode = 1

while True:
   try:
       i_num = int(input("Enter a number: "))
       if i_num < number:
           raise ValueTooSmallError
           
       elif i_num > number:
           raise ValueTooLargeError
       break
   except ValueTooSmallError:
       print("This value is too small, try again!")
       print(exitcode)
       #sys.exit("Error message")
   except ValueTooLargeError:
       print("This value is too large, try again!")
       print()

print("Congratulations! You guessed it correctly.")




