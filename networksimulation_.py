# -*- coding: utf-8 -*-
"""
Created on Wed Jul 04 15:59:31 2018

@author: christian.hibbert
"""

__metaclass__ = type
from math import sqrt
from math import cos
from math import sin
from math import pi
from math import atan
from random import *
import math
import csv
import logging
from test import Dict
from DataFiles3_ import ResultsFile
#from storagedevice import SubstationESS
#from storagedevice import NetworkESS
from datamovement import IPSAModel
#from datamovement import LoadDivision
##from datamovement import PowerData
#from datamovement import WriteResults
from networkobservation import StatutoryChecks
from controlactions import ControlAction 
##from ipsa import IscInterface
##from ipsa import IscNetwork
##from ipsa import IscAnalysis
##from ipsa import IscLoad
##from ipsa import IscBranch
##from ipsa import IscSynMachine
##from ipsa import IscTransformer
import ipsa
#import utilities
myLogger = logging.getLogger('default')


class LoadFlowModel: # we'll inherit LoadFlowModel in LoadFlowControl
    """
    Controls calls to the load flow model.
    """
    def __init__(self):
        """
        On intialisation makes connection with IPSA and acquires load/transformer info
        """

        im = IPSAModel(self.config)
        fileName = im.getFileLoc()
        myLogger.info('-+- Using model:' + fileName)
        del im
##        self.ipsasys = ipsaIscInterface()
        self.ipsasys = ipsa.GetScriptInterface()
        self.net = self.ipsasys.ReadFile(fileName)
        self.ipsaloads = self.net.GetLoads()
        self.trans = self.net.GetTransformers()
        self.getbuses = self.net.GetBusbars()
        self.ipsaanalysis = self.net.GetAnalysisLF()
        #self.tTapstart = self.config.get('transformerpos', 'tapStart')
        
        
        
        
       
#        for option in self.net.GetTransformers():
#            if option == self.config.get('transformers', 't1'):
#                tx = self.net.GetTransformer(option)
#                tx.SetDValue(ipsa.IscTransformer.TapStartPC, eval(tTapstart))
###                print "set tap start v1",tx.SetDValue(ipsa.IscTransformer.TapStartPC, eval(tTapstart))
###                tx.SetDValue(ipsa.IscTransformer.SpecVPU, 0)

##    def __del__(self):
###        NetworkSimulation.__del__(self)
##        self.ipsasys.DeleteNetwork(self.net)
##        del self.ipsasys
##        myLogger.info('-+- removed IPSA model from memory -+-')
    

    def insertValues(self, powerKeys, scaleFact): 
        # need another method if considering load profiles maybe or be selective with scaleFact etc - let this be an if statement from wherever it was called 
        
        #scaleFact = self.config.getfloat('loads', 'scaleFact') #1.5 # [config] self.config
        
        loads = self.net.GetLoads() # set the loads through linear distribution 
        disVals = self.config.getint('loads', 'disVals') #100          #discrete values
        PF = self.config.getfloat('loads', 'PF') # used elsewhere
        
        for load in loads.itervalues(): #Iterate all loads across the network
            varName = load.GetName() #'Bus1', 'Bus2' etc
            
            val = powerKeys[varName.split(".")[1]]
            
            
            if scaleFact == 0:
                kVA = float(val)/1000
            else:
                val = float(val)/1000# * scaleFact
                # Discrete load calculation
                kVA = val#/disVals # would'nt need this if np was working 
                #discretescale = randint(0, disVals)
                #kVA = kVA * discretescale            
            
            P = kVA*PF
            
            if PF == 1 or P ==0:
                Q = 0
            else:
                Q = math.sqrt(kVA**2-P**2)
            
            load.SetDValue(ipsa.IscLoad.RealMW,P)    
            load.SetDValue(ipsa.IscLoad.ReactiveMVAr,Q)
    
    
    def getLoads(self):
        
        self.ApparentPowerLoads = {}
        loads = self.net.GetLoads() # set the loads through linear distribution  
        
        for load in loads.itervalues(): #Iterate all loads across the network
            varName = load.GetName()
            #print load.GetName(), load.GetPowerMagnitudekVA()
            self.ApparentPowerLoads[varName.split(".")[1]] = load.GetPowerMagnitudekVA()
            
        return self.ApparentPowerLoads 
    
    
    def unlockTaps(self):
        """
        locks transformer taps when using ESS for intervention and when
        normal tap control in IPSA does not work to solve problem
        """

        print "tap unlocked in sim", self.ipsaanalysis.SetIValue(ipsa.IscAnalysisLF.LockTaps, 1)
##        return lf.SetIValue(ipsa.IscAnalysisLF.LockTaps, 2)
        return self.ipsaanalysis.SetIValue(ipsa.IscAnalysisLF.LockTaps, 1)

    def doLoadFlow(self):
        """
        Calls IPSA to run a load flow
        """
        return self.net.DoLoadFlow()

#    def getNetRef(self):
#        """
#        Makes the handle from the simulation available outside this class.
#        """
#        return self.ipsasys, self.net

class CollectNetStatus: # CollectNetStatus is not inherited....I don't think..
    """
    Reads conditions on network following a load flow analysis.
    """
#    def __init__(self, simulation, config):
    def __init__(self, results, net, config):
#        self.config = config
        #self.checker = StatutoryChecks(self.config)
        ####self.resultlog = WriteResults(config)
        # self.ipsasys, self.net = simulation.getNetRef()
        #self.ResultDict = Dict()

        self.config = config
        # Feeder information - busbars on fedderID
        
        self.net= net
        #self.tx1 = self.net.GetTransformer(config.get('transformers', 't1'))
        #print "transformer 1",self.tx1
        #self.lastTap1 = round(self.tx1.GetDValue(ipsa.IscTransformer.TapStartPC),4)
        #print " ********************** Initial TapStartPC tx1 and tx2:", self.lastTap1
        #self.nomvolts = config.getfloat('netobj', 'nomvolts')
        self.nomvolts = self.config.getfloat('characteristics', 'nominalVoltagekV') #11.0 # this could come from config file...
        self.gridInfeed = self.config.getfloat('characteristics', 'gridInfeed') #11.0 # this could come from config file...
        #self.getNetParams()
        self.results = results
    #def getNetParams(self, sd, sd2):
    def getNetParams(self,t):
        
        """
        Reads network conditions following a load flow analysis.
        Focus is on the voltages for now... 
        
        """
        self.ResultDict = Dict()
        
        # Get voltages....
        
        self.getbuses = self.net.GetBusbars()
        for bus in self.getbuses.itervalues():
            
            name = bus.GetName()
            if name != 'Infeedbusbar':
                vm = bus.GetVoltageMagnitudekV()
                print(name, vm)        
                self.ResultDict.update_dict({name:vm}) # this is a one key dictionary 
            elif name == 'Infeedbusbar':
                if not bus.GetVoltageMagnitudekV() == self.gridInfeed:
                    raise Exception("Infeed voltages are not the same!")
                        
            
#                except VoltagesAreNotEqual:
#                    print("Infeed voltages are not the same!")
#                    print(exitcode)
            
        #xxxxxxxx
        # Compute any reverse power flows..
        
        # 
        
        # There must be some decision making strategy as in what to send back to _processRow...
        # 1. Is the health of the system OK - check voltages against nominal?
        #           if so then write results to file
        #           else return prognosis back to _processRow
        
        resultlog, vLimits = self._makePc(t)
        
        # now all voltages have been obtained we can do some stuff...
        
        # output results if the network is good


        return resultlog, vLimits
        
       
#    def countLosses(self):
#        """
#        Sum up transmormer and network losses
        
    #def _makePc(self, voltage):
    def _makePc(self,t):
        """
        Return a voltage as a percent value of nominal.
        """
        
        #self.NomVolts = {}
#        for key, val in self.ResultDict.myDict.iteritems():
#            self.NomVolts[key] = val
        self.NomVolts = self.ResultDict.myDict.copy()
        self.NomVolts.update((x, ((y-self.nomvolts)/self.nomvolts+1)) for x, y in self.NomVolts.items()) # per unit system       
        
        if any(item > 1.06 for item in self.NomVolts.values()) == True or any(item < 0.94 for item in self.NomVolts.values()) == True: #any(0.94 < item < 1.06 for item in self.NomVolts.values()) == True:           # test on the per unit system with +/- 6%
            total = len([i for i, e in enumerate(self.NomVolts.values()) if not 0.94 <= e <= 1.06]) # how many occurances
            #resultlog = sum([i for i, e in enumerate(NomVolts.values()) if 0.94 <= e <= 1.06])
            underV = len([i for i, e in enumerate(self.NomVolts.values()) if e <= 0.94]) # how many occurances
            overV = len([i for i, e in enumerate(self.NomVolts.values()) if e >= 1.06]) # how many occurances
            
            resultlog = {'underV':underV, 'overV':overV, 'total':total}
        else:
            resultlog = {'underV':0, 'overV':0, 'total':0}
            
        # this assumes we are writing all results regardless of system fix...
        self.time_step = 0
        writer = 'writ' + str(self.time_step)
        self.results.config_headers
        li_row = [str(v) for v in self.NomVolts.values()]
        li_row.insert(0,t)
        self.results.writeRow(writer, li_row)
        #self.results.writeRow(writer, [str(v) for v in self.NomVolts.values()])
        
        #python data frame for a given feeder and a given time
        # feederID | time | # volts magnitudes
        
        # SQL based - open channel at the start of the program...
        
#        print(self.ResultDict.myDict['Infeedbusbar'])
#        print('Infeedbusbar' in self.ResultDict.myDict)
#        #maxV = max((value for key, value in self.ResultDict.myDict.values() if key != 'Infeedbusbar'))#, csr_matrix((3, 3)))
#        seq = [value for key, value in self.ResultDict.myDict.items() if key is not 'Infeedbusbar']
#        print(seq,max(seq))
#        xxx
        
        #vLimits = (min(self.ResultDict.myDict.values()),maxV)# min and max voltage
        vLimits = (min(self.ResultDict.myDict.values()),max(self.ResultDict.myDict.values()))# min and max voltage
        
        
        return resultlog, vLimits  #, self.NomVolts #round(100.0 * (voltage / self.nomvolts - 1),2)
    
    
    
    def voltsList(self,a): # only obtain list of voltage magnitudes once 
        """ obtain list of voltage magnitudes once sure network condition satisfied """
        
        NomVolts = self.NomVolts
        print NomVolts
        xxxxx
        return NomVolts
    