# -*- coding: utf-8 -*-
"""
Created on Wed Jul 04 19:00:22 2018
Purpose:     Observing various defined events on the network
@author: christian.hibbert
"""

__metaclass__ = type

##from ipsa import IscBranch
##from ipsa import IscSynMachine
import ipsa
from math import sqrt
from math import atan
from math import cos
##from loadflowcontrol import *
#from datamovement import LoadDivision

class NetworkObservation:
    """
    Base class for objects that require measurements from the network.
    """

class OutOfLimitEvent(NetworkObservation):
    def __init__(self, config):
        self.ool = False
        self.whichThresh = None
        self.config = config
        self.margin = None
        self.meas = {}

    def setEventType(self, event):
        self.eventType = event
        print '  *** Set event type to:', event
        return True

    def getEventType(self):
        return self.eventType

    def setLocation(self, location):
        self.location = location
        print '  *** Set location to:', location
        return True

    def getLocation(self):
        return self.location

    def setMargin(self, margin):
        self.margin = margin
        print '  *** Set margin to:', margin
        return True

    def getMargin(self):
        return self.margin

    def getMeas(self):
        return self.meas

    def getOolState(self):
        return self.ool

    def test (self, simulation, sd, sd2):
        """
        Select requested intervention (deleted self.ipsasys)???
        """
        ipsasys, self.net = simulation.getNetRef()
        self.sd = sd
        self.sd2 = sd2
        self.ool = False
        method = getattr(self, self.eventType, None)
        if callable(method):return method()
        else: raise Exception ('selected event not found')


    def setThreshold(self, threshold):
        """
        Sets object event threshold
        """
        self.threshold = threshold
        print '  *** Set threshold to:', threshold
        return True

    def getThreshold(self):
        """
        retrieve the event threshold
        """
        return self.threshold
    def getTfmrState(self):
        """"
        get transformer status
        """
        return self.tapmove

##    def overvolt(self):
##        """
##        checks for voltages on all busbars above 6% threshold?
##        """
##        busbars = self.net.GetBusbars()
##        for bus in busbars.itervalues():
##            name = bus.GetName()
##            vm = bus.GetVoltageMagnitudePU()
##            vpc = 100*(vm - 1)
##            self.meas.update({'vpc':vpc})
##            print 'vpc:', vpc
##            if vpc > self.threshold:
##                self.ool = True
##                print "overvoltage problem", name, vm
##            return self.ool
##
##
##    def undervolt(self):
##        """
##        checks for voltages on all busbars above 6% threshold?
##        """
##        busbars = self.net.GetBusbars()
##        for bus in busbars.itervalues():
##            name = bus.GetName()
##            vm = bus.GetVoltageMagnitudePU()
##            vpc = 100*(vm - 1)
##            self.meas.update({'vpc':vpc})
####            print 'vpc:', vpc
##            if vpc < self.threshold:
##                self.ool = True
##                print "undervoltage problem", name, vm
##            return self.ool

    def overvolt(self):
        """
        checks for voltages on all busbars above 6% threshold?
        """
        busbars = self.net.GetBusbars()
        ov_count = 0
        ov_max = 0.0
        ov_events = []
        time_count =0
##        upper_threshold = 5.99

        for bus in busbars.itervalues():
            name = bus.GetName()
            vm = bus.GetVoltageMagnitudePU()
            vpc = 100*(vm - 1)
            self.meas.update({'vpc':vpc})
##            print 'vpc:', vpc
            # test condition for out-of-limits and puts recorded overvoltage
            #values in a list
##            if self.tapWaiiting == 1:
            if vpc > self.threshold:
    ##                print "this is threshold for ov",self.threshold
    ##                self.ool = True
                ov_count = ov_count + 1
                ov_events.append(vpc)
##                print 'overvoltage magnitude:', vpc, "no. overvoltage\
##                occurences:", ov_count
            else:
                pass
        # gets the maximum overvoltage value and uses that to
        # to calculate the margin used for the ESS operation
        if len(ov_events) >= 1:
           self.ool = True
           print "overvoltage ool", self.ool
           for num in ov_events:
                if num > ov_max:
                    ov_max = num
           print " the maximum overvoltage value is:", ov_max
##                print ov_max
##           PU_OVmax = 1 + (ov_max/100.0)
##           new_thresh = 1 + (self.threshold/100.0)
           # set margin using calculated VSF for required Q
##           self.setMargin((new_thresh - PU_OVmax)/0.031263)
##           self.setMargin(self.threshold - ov_max)
##        self.setMargin((self.threshold - ov_max)/6)
##           del num, bus, name, vm, vpc
        return self.ool

##    def overvolt(self):
##        """
##        checks for voltages above threshold for location specified in config file??
##        """
##        busDG = self.net.GetBusbar(self.location)
##        print busDG
##        vDG = busDG.GetVoltageMagnitudePU()
##        vpc = 100 * (vDG-1)
##        self.meas.update({'vpc':vpc})
##        print 'vpc:', vpc
##        if vpc > self.threshold: self.ool = True
##        return self.ool
##

    def undervolt(self):
        """
        checks for voltages on all busbars above 6% threshold?
        """
        busbars = self.net.GetBusbars()
        uv_count = 0
        uv_min = 0.0
        uv_events = []
##        lower_threshold = -5.99
        for bus in busbars.itervalues():
            name = bus.GetName()
            vm = bus.GetVoltageMagnitudePU()
            vpc = 100*(vm - 1)
            self.meas.update({'vpc':vpc})
##            print 'vpc:', vpc
            # test condition for out-of-limits and puts recorded undervoltage
            #values in a list
##            if self.tapWaiting == 1:
            if vpc < self.threshold:
    ##                print "this is threshold for uv",self.threshold
    ##                self.ool = True
                uv_count = uv_count + 1
                uv_events.append(vpc)
    ##                print 'undervoltage magnitude:', vpc, "no. of undervoltage\
    ##                occurences:", uv_count
            else:
                pass
        # gets the minimum undervoltage value and uses that to
        # calculate the margin used for the ESS operation
        if len(uv_events) >= 1:
           self.ool = True
           print "undervoltage ool", self.ool
           for num in uv_events:
                if num < uv_min:
                    uv_min = num
           print " the minimum undervoltage value is:", uv_min
##           PU_UVmin = 1 + (uv_min/100.0)
##           new_thresh = 1 + (self.threshold/100.0)
           # set margin using calculated VSF for required Q
##           self.setMargin((new_thresh - PU_UVmin)/0.031263)
##           self.setMargin((self.threshold - uv_min)/6)
##           del num, bus, name, vm, vpc
##        print "this is undervolt ool",self.ool
        return self.ool
##    def undervolt(self):
##        """
##        checks for voltages above threshold for location specified in config file??
##        """
##        busDG = self.net.GetBusbar(self.location)
##        print busDG
##        vDG = busDG.GetVoltageMagnitudePU()
##        vpc = 100 * (vDG-1)
##        self.meas.update({'vpc':vpc})
##        print 'vpc:', vpc
##        if vpc < self.threshold: self.ool = True
##        return self.ool

# THIS CHECKS REVERSE POWER FLOW ACROSS DIFFERENT LINES ON THE NETWORK
##    def reversepowerflow(self):
##        """
##        checks for reverse power onto the feeder below threshold??
##        """
##        feeder = self.net.GetBranches()
##        # allow 0.01 MW tolerance
##        tol = 0.01
##        rpf_count = 0
##        rpf_max = 0.0
##        rpf_events = []
##        for feeder in feeder.itervalues():
##            pfeed = feeder.GetSendRealPowerMW()
####            print 'pfeed:', pfeed
##            # test condition for out-of-limits and puts recorded reverse power
##            #flow values in a list
##            if pfeed + tol < self.threshold:
####                print "this is threshold for rvp",self.threshold
####                self.ool = True
##                rpf_count = rpf_count + 1
##                rpf_events.append(pfeed)
####                print 'reverse power flow magnitude:', pfeed, "no. of reverse\
####                power flow events", rpf_count
####        print "reverse power flow events", rpf_events
##        # gets the maximum reverse power flow value and uses that to\
##        # calculate the margin used for the ESS operation
##        if len(rpf_events) >= 1:
##           self.ool = True
####           print "this is rvp ool",self.ool
##           for num in rpf_events:
##                if num < rpf_max:
##                    rpf_max = num
##                    print " !!!!!!!RPFMAX ", abs(rpf_max)
####           print " the maximum reverse power flow value is:", abs(rpf_max)
##           self.setMargin(self.threshold - rpf_max)
##           del num, feeder, pfeed
##        return self.ool

    def reversepowerflow(self):
        """
        checks for reverse power onto the feeder below threshold??
        """
        feeder = self.net.GetBranch(self.location)
        pfeed = feeder.GetSendRealPowerMW()
        #feeder = self.net.GetGridInfeed(self.location)
        #pfeed = feeder.GetRealPowerMW()
        print ' checking reversepower flow (-ve) if true, pfeed:', pfeed
        # allow 0.01 MW tolerance
        tol = 0.01
        # test condition for out-of-limits
        if pfeed + tol < self.threshold:
            self.ool = True
            newNum = (self.threshold - pfeed)*1.2
            self.setMargin(newNum)
        print "there is reverse power flow", self.ool
        return self.ool

    def branchoverpower(self):
        """
        Looks for power flow above threshold and sets margin.
        """
        branches = self.net.GetBranches()
        bovpower_count = 0
        bovpower_max = 0.0
        bovpower_events = []
        bovpower_rating = []
        for branch in branches.itervalues():
            # this branch is not considered, just used to represent grid infeed
            # note though overpower is recorded here, it is not recorded as an ool event
            if branch.GetName() == "Infeedbus.Grid.InfeedPower":
                pass
            else:
##                print "testing branch name output", branch.GetName()
                bmva = branch.GetSendPowerMagnitudeMVA()
##                print "branch magnitude", bmva
            #for ipsa 1.6
##            bmvarate = branch.GetDValue(ipsa.IscBranch.StdMVA)
                bmvarate = branch.GetRatingMVA(0)
##                print "branch rating",bmvarate
                mvapc = 100 * bmva / bmvarate
##            print "percentage power output", mvapc
            # allow 1pc tolerance
                tol = abs(mvapc) * 0.01
            # test condition for out-of-limits
##            print 'mvapc:', mvapc
            # test condition for out-of-limits and puts recorded overpower
            # values in a list
                if mvapc - tol > self.threshold:
##                print "this is threshold for ovp",self.threshold
##                self.ool = True
                    bovpower_count = bovpower_count + 1
                    bovpower_events.append(mvapc)
                    bovpower_rating.append(bmvarate)
##                    print 'over power magnitude:', mvapc, "no. of over power\
##                    events", bovpower_count

##        print "overpower events branch", bovpower_events, bovpower_rating

        # gets the maximum over power value and uses that to\
        # calculate the margin used for the ESS operation
        if len(bovpower_events) >= 1:
            self.ool = True
            print "overpower ool", self.ool
            for num in bovpower_events:
##                print "!!!!!!!!!!!!!testing123, ",num, ovpower_events, ovpower_max
                if num > bovpower_max:
                    bovpower_max = num
##                    print " !!!!!!!!ovpowermax", ovpower_max, ovpower_count
            max_rating = max(bovpower_events)
            max_index = bovpower_events.index(max_rating)
            print " the maximum overpower value is and bmvarate:", bovpower_max, bovpower_rating[max_index]
            self.setMargin(((bovpower_max / self.threshold) - 1.0) * bovpower_rating[max_index])
##           del num, branch, bmva, bmvarate, mvapc, tol
##           print "this is overpower ool",self.ool
        return self.ool

    def tfmroverpower(self):
        """
        Looks for power flow above threshold and sets margin.
        Note: check if there is n-1 transformers. The power flow through each
        transformer would be the total power flow through the grid.
        """
        tfmrs = self.net.GetTransformers()
        ovpower_count = 0
        ovpower_max = 0.0
        ovpower_events = []
        ovpower_rating = []
        for tfmr in tfmrs.itervalues():
##            print branch.GetName()
            bmva = tfmr.GetSendPowerMagnitudeMVA()
##            print "tfmr magnitude", bmva
            #for ipsa 1.6
##            bmvarate = branch.GetDValue(ipsa.IscBranch.StdMVA)
            bmvarate = tfmr.GetRatingMVA(0)
##            print "tfmr rating",bmvarate
            mvapc = 100 * bmva / bmvarate
##            print "percentage power output", mvapc
            # allow 1pc tolerance
            tol = abs(mvapc) * 0.01
            # test condition for out-of-limits
##            print 'mvapc:', mvapc
            # test condition for out-of-limits and puts recorded overpower
            # values in a list
            if mvapc - tol > self.threshold:
##                print "this is threshold for ovp",self.threshold
##                self.ool = True
                ovpower_count = ovpower_count + 1
                ovpower_events.append(mvapc)
                ovpower_rating.append(bmvarate)
                print 'over power magnitude:', mvapc, "no. of over power\
                events", ovpower_count
##        print "overpower events transformer events and rating", ovpower_events, ovpower_rating

        # gets the maximum over power value and uses that to\
        # calculate the margin used for the ESS operation
        if len(ovpower_events) >= 1:
           self.ool = True
           print "overpower ool", self.ool
           for num in ovpower_events:
##                print "!!!!!!!!!!!!!testing123, ",num, ovpower_events, ovpower_max
                if num > ovpower_max:
                    ovpower_max = num
##                    print " !!!!!!!!ovpowermax", ovpower_max, ovpower_count
           max_rating = max(ovpower_events)
           max_index = ovpower_events.index(max_rating)
           print " the maximum overpower value is and bmvarate:", ovpower_max, ovpower_rating[max_index]
           self.setMargin(((ovpower_max / self.threshold) - 1.0) * ovpower_rating[max_index])
##           del num, tfmr, bmva, bmvarate, mvapc, tol
##           print "this is overpower ool",self.ool
        return self.ool


    def lowutil(self):
        """
        checks for underutilisation and then discharges storage
        """
        tfmrs = self.net.GetTransformers()
        upower_count = 0
        upower_max = 0.0
        upower_events = []
        upower_rating = []
        for tfmr in tfmrs.itervalues():
##            print branch.GetName()
            bmva = tfmr.GetSendPowerMagnitudeMVA()
##            print "tfmr magnitude", bmva
            #for ipsa 1.6
##            bmvarate = branch.GetDValue(ipsa.IscBranch.StdMVA)
            bmvarate = tfmr.GetRatingMVA(0)
##            print "tfmr rating",bmvarate
            mvapc = 100 * bmva / bmvarate
##            print "percentage power output", mvapc
            # allow 1pc tolerance
            tol = abs(mvapc) * 0.01
            # test condition for out-of-limits
##            print 'mvapc:', mvapc
            # test condition for out-of-limits and puts recorded overpower
            # values in a list
            if mvapc - tol > self.threshold:
##                print "this is threshold for ovp",self.threshold
##                self.ool = True
                upower_count = ovpower_count + 1
                upower_events.append(mvapc)
                upower_rating.append(bmvarate)
                print 'over power magnitude:', mvapc, "no. of over power\
                events", ovpower_count
        print "overpower events transformer events and rating", ovpower_events, ovpower_rating

        # gets the maximum over power value and uses that to\
        # calculate the margin used for the ESS operation
        if len(upower_events) >= 1:
           self.ool = True
           print "transformer underutilised ool", self.ool
           for num in ovpower_events:
##                print "!!!!!!!!!!!!!testing123, ",num, ovpower_events, ovpower_max
                if num > upower_max:
                    upower_max = num
##                    print " !!!!!!!!ovpowermax", ovpower_max, ovpower_count
           max_rating = max(upower_events)
           max_index = upower_events.index(max_rating)
           print " the maximum overpower value is and bmvarate:", upower_max, upower_rating[max_index]
           self.setMargin(((upower_max / self.threshold) - 1.0) * upower_rating[max_index])
##           del num, tfmr, bmva, bmvarate, mvapc, tol
##           print "this is overpower ool",self.ool
        return self.ool

    def overpower(self):
        """
        Looks for power flow above threshold and sets margin.
        """
        branch = self.net.GetBranch(self.location)
##        print "this is the branch", branch
        bmva = branch.GetSendPowerMagnitudeMVA()
##        bmvarate = branch.GetDValue(IscBranch.StdMVA)
        bmvarate = branch.GetRatingMVA(0)
        mvapc = 100 * bmva / bmvarate
        print" branch power and rating", bmva, bmvarate
        # allow 1pc tolerance
        tol = abs(mvapc) * 0.01
        # test condition for out-of-limits
        print 'mvapc:', mvapc
        if mvapc - tol > self.threshold: self.ool = True
        self.setMargin((mvapc / self.threshold - 1) * bmvarate)
        return self.ool

##    def discharge(self):
##        #location of ESS used for pk shaving
####        ess = self.net.GetSynMachine(self.config.get('generators', 'ess'))
##        branch = self.net.GetBranch(self.location)
##        bmva = branch.GetSendPowerMagnitudeMVA()
##        bmvarate = branch.GetRatingMVA(0)
##        mvapc = 100 * bmva / bmvarate
##        # allow 1pc tolerance
##        tol = abs(mvapc) * 0.01
##        if mvapc - tol < self.threshold:
##            print "discharge can take place"
##            self.setMargin



    def reactivepowerflow(self):
        """
        checks for reactive power flow onto feeder above threshold??
        """
        branch = self.net.GetBranch(self.location)
        #print branch
        qbranch = branch.GetSendReactivePowerMVAr()
        print 'reactive power flow qbranch:', qbranch
        # allow 1pc tolerance
        tol = abs(qbranch) * 0.01
        # test condition for out-of-limits
        if qbranch - tol > self.threshold: self.ool = True
        self.setMargin(qbranch - self.threshold)
        return self.ool

    def excesspgeneration(self):
        """
        Looks for generation exceeding threshold
        """
        dg = self.net.GetSynMachine(self.location)
        ess = self.net.GetSynMachine(self.config.get('generators', 'ess'))
        pdg = dg.GetDValue(ipsa.IscSynMachine.GenMW)
        pess = ess.GetDValue(ipsa.IscSynMachine.GenMW)
        # need to remove pess from calculation when doing restore operation
        # in such a case the pess will be >0, so do the following:
        if pess > 0: pess = 0
        print 'excess generation pdg:', pdg, '/ pess: ', pess
        # allow 1pc tolerance
        tol = abs(pdg) * 0.01
        # test condition for out-of-limits
        if pdg + pess - tol > self.threshold: self.ool = True
        self.setMargin(pdg - self.threshold)
        return self.ool

    def excessqdemand(self):
        """
        Looks for MVAr demand exceeding threshold.
        """
        dg = self.net.GetSynMachine(self.location)
        qdg = dg.GetDValue(ipsa.IscSynMachine.GenMVAr)
        print 'excess reactive power demand qdg', qdg
        # allow 1pc tolerance
        tol = abs(qdg) * 0.01
        # test condition for out-of-limits
        ESS_P, ESS_Q = self.sd.getPower()
        if qdg + tol + ESS_Q < self.threshold: self.ool = True
        self.setMargin(self.threshold - qdg)
        return self.ool

    def percentage(part, whole):
        return 100 * float(part)/float(whole)

class StatutoryChecks (NetworkObservation):
    """
    Independent checks that are not based on artificially imposed limits.
    """

    def __init__(self, config):
        self.config = config

    def doVoltCheck(self,net):
        """
        Checks for +/-6% voltage excursion.
        """
        busbars = net.GetBusbars()
        ex_volts = 0
        ex_volts_err = ''
        vpc_max = 0
        v_max_loc = None
        for bus in busbars.itervalues():
            name = bus.GetName()
            vm = bus.GetVoltageMagnitudePU()
            vpc = 100*abs(vm-1)
            vpcws = 100*(vm-1)
            if vpc > vpc_max:
                vpc_max = vpc
                vpcws_max = vpcws
                v_max_loc = name
            if vpc > 6.0:
                ex_volts = 1
                ex_volts_err = ex_volts_err + repr(name) + ', ' + \
                    repr(vpcws) + ', '
        if ex_volts == 1:
##            print "$$$$$$ voltage excursion list",round(vpcws_max,2), ex_volts_err
            returnList = [round(vpcws_max,2), ex_volts_err]
        else:
##            print "$$$$$$no voltage excursion list",round(vpcws_max,2), v_max_loc
            returnList = [round(vpcws_max,2), v_max_loc]
##        print "testing voltage excursion", returnList
        return returnList


    def doThermalCheckA(self, net):
        """
        checks proximity of assets (branches) to thermal limits using 75% as threshold
        """
        branches = net.GetBranches()
        ex_mva = 0
        ex_mva_err = ''
        mvapc_max = 0
        mva_max_loc = None
        for branch in branches.itervalues():
            name = branch.GetRealName()
##            print "net observe name", name
            if name[:1] != 't':
##                print " thermal check", name
##                bmvarate = branch.GetDValue(ipsa.IscBranch.StdMVA)
                bmvarate = branch.GetRatingMVA(0)
##                print " thermal check branch mva rate", bmvarate
                bmva = branch.GetSendPowerMagnitudeMVA()
##                print " thermal check branch mva sent", bmva
                if bmvarate > 0:
                    mvapc = 100 * bmva / bmvarate
                else:
                    mvapc = 0
                if mvapc > mvapc_max:
                    mvapc_max = mvapc
                    mva_max_loc = name
                if mvapc > 75:
                    ex_mva = 1
                    ex_mva_err = ex_mva_err + repr(name) + ', ' + \
                        repr(mvapc) + ', '
            else:
                Pass
        if ex_mva == 1:
            returnList = [round(mvapc_max,1), ex_mva_err]
        else:
            returnList = [round(mvapc_max,1), mva_max_loc]
##        print "testing thermal excursion",returnList
        return returnList

    def busVSFCheck(self, net):
        """
        checks voltage stability of assets at every load flow
        """

        busbars = net.GetBusbars()
        #vSub = 0
        #VBus = 0
        #v_bus_val = []
        vsf_min_loc = None
        vsf_max_loc = None
        vsub_loc = None
        vsf_min = 0.94
        vsf_max = 1.06
        for sub in busbars.itervalues():
            subname = sub.GetName()
            if subname == self.config.get('subvoltage', 'voltage'):
                vSub = sub.GetVoltageMagnitudePU()
                vsub_loc = subname

##        for sub in self.config.options('subvoltage'):
##            name = sub.GetName()
##            vBus = sub.GetVoltageMagnitudePU()
##            subName = vBus_loc

        for bus in busbars.itervalues():
            name = bus.GetName()
            vBus = bus.GetVoltageMagnitudePU()
            vsf = (vBus*2)-vSub
            #print "testing VSF", vsf
            if vsf < vsf_min:
##                print "this is vBus and vSub", vBus, vSub
                vsf_min = vsf
                vsf_min_loc = name
            elif vsf > vsf_max:
                vsf_max = vsf
                vsf_max_loc = name
##            print "testing VSF", vsf_max_loc, vsf_min_loc
        returnList = [round(vsf_min,5), vsf_min_loc, round(vsf_max,5), vsf_max_loc]
        return returnList
    def timeDelay(self, net):
        """
        Time delay setting for ESS and OLTC action
        """



##    def doThermalCheckB(self, net):
##        """
##        checks proximity of assets to thermal limits using 90% as threshold
##        """
##        branches = net.GetBranches()
##        ex_mva = 0
##        ex_mva_err = ''
##        mvapc_max = 0
##        mva_max_loc = None
##        for branch in branches.itervalues():
##            name = branch.GetRealName()
##            if name[:2] != 't2':
##                print "testing t2", name
##                bmvarate = branch.GetDValue(IscBranch.StdMVA)
##                bmva = branch.GetSendPowerMagnitudeMVA()
##                if bmvarate > 0:
##                    mvapc = 100 * bmva / bmvarate
##                else:
##                    mvapc = 0
##                if mvapc > mvapc_max:
##                    mvapc_max = mvapc
##                    mva_max_loc = name
##                if mvapc > 90:
##                    ex_mva = 1
##                    ex_mva_err = ex_mva_err + repr(name) + ', ' + \
##                        repr(mvapc) + ', '
##        if ex_mva == 1:
##            returnList = [round(mvapc_max,1), ex_mva_err]
##        else:
##            returnList = [round(mvapc_max,1), mva_max_loc]
##        return returnList
##


##    def overvolt(self):
##        """
##        Reads voltage drop conditions on network following a load flow analysis.
##        """
##        busbars = self.net.GetBusbars()
##        for bus in busbars.itervalues():
####            print bus
##            uv_pu = round(bus.GetVoltageMagnitudePU(),3)
##            uv_pc = 100*(uv_pu-1)
##            uthreshold = -6.0
##            if uv_pc < uthreshold:
##                print "undervoltage %f" %(uv_pc)
##                self.ool= True
##            return self.ool
####                print self.ool
##
##    def undervolt(self):
##        """
##        Reads voltage drop conditions on network following a load flow analysis.
##        """
##        for bus in busbars.itervalues():
##            ov_pu = round(bus.GetVoltageMagnitudePU(),2)
##            ov_pc = 100*(ov_pu-1)
##            othreshold = 6.0
##            if ov_pc > othreshold:
##                print "overvoltage %f" %(ov_pc)
##                self.ool= True
##            return self.ool
####                print self.ool


