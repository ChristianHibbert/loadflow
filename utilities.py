# -*- coding: utf-8 -*-
"""
Created on Wed Jul 04 16:49:11 2018

@author: christian.hibbert
"""

__metaclass__ = type

from datetime import datetime
#from datamovement import WriteResults
from winsound import Beep

class Utilities:
    """
    Base class for utility functions.
    """

class TimeRun(Utilities):
    """
    Methods to record the elapsed time during the analysis.
    """
    def startLog(self):
        self.startTime = datetime.now()
        print self.startTime.strftime("Started at %X, %x")
        return True
    def endLog(self, config):
        # Calculate elapsed time for simulation
        endTime = datetime.now()
        diff = endTime - self.startTime
        duration = diff.seconds
        lines = [['duration (s)', duration, 'Run Completed',
                        endTime.strftime("%X"), endTime.strftime("%x")]]
        datalog = WriteResults(config)
        # record events and actions
        for n in range(11):
            lines.append([''])
        for str in config.sections():
            if str[:5] == 'event':
                lines.append([config.getint(str, 'priority'),
                config.get(str, 'event'),
                config.get(str, 'location'),
                config.getfloat(str, 'threshold'),
                config.get(str, 'action')])
        lines.append([''])
        lines.append(['IPSA model', config.get('files', 'model')])
        if not datalog.writeRows(lines): raise Exception('datalog failed')
        # record events and actions
        return True

class Notify(Utilities):
    """
    Alert user that run is complete
    """
    def alert(self):
        # time
        bpm = 300
        tC = 1000 * 60 / bpm
        tT = 2 * tC / 3
        tQ = tC / 2
        tM = tC * 2
        # pitch
        pCn0 = 262 # 261.626
        pCs0 = 277 # 277.183
        pDn0 = 294 # 293.665
        pFs0 = 370 # 369.994
        pAn0 = 440
        pCs1 = 554
        pFs1 = 740

        f1 = pFs0
        f3 = pAn0
        f5 = pCs1
        f11 = pFs1
##        tune = [[f1, tT], [f3, tT], [f5, tT], [f11, tC], [f5,tQ], [f3,tQ],
##                [f1, tC]]
        tune = [[f1, tT], [f5, tT], [f3, tT], [f11, tQ]]
        for note in tune:
            Beep(note[0], note[1])
##        Beep(f3, 100)
##        Beep(f5, 100)
##        Beep(f11, 100)
##        Beep(f5, 100)
##        Beep(f3, 100)
##        Beep(f1, 100)

##            f = f * 2


##class MarketControl(Utilities):