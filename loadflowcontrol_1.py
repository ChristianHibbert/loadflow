# -*- coding: utf-8 -*-
"""
Created on Wed Jul 04 15:47:57 2018

# Name:       loadflowcontrol
# Purpose:    Controls the all aspects of the simulation

@author: christian.hibbert
"""

from __future__ import division
__metaclass__ = type
#from storagedevice import SubstationESS
#from storagedevice import NetworkESS
#from networkobservation import *
#from controlactions import *
from datamovement import *
from networksimulation_ import *
##from ipsa import IscInterface
##from ipsa import IscTransformer
import ipsa
import datetime
from test import Dict
from DataFiles3_ import * #ResultsFile #writeResults





class loadFlowControl(LoadFlowModel):
    """
    Coordinates objects to provide annual simulation
    """
    def __init__(self, config, mainCount, results):
        """
        Main loop initialisation.
        """
        #print 'this is mainCount', mainCount
        self.mainCount = mainCount
        print('Enetered loadFlowControl')
        self.config = config # create an instance of the config parser
        self.datasource = LoadCapacity(config, mainCount) # 
        
        print(self.datasource.BusLoads)
        
        self.PF = config.getfloat('loads', 'PF') #0.98                              # get from config file
        self.weeks = config.getint('simulation', 'weeks')
        super(loadFlowControl, self).__init__()
#        super(CollectNetStatus, self).__init__()
        self.scaleFact = self.config.getfloat('loads', 'scaleFact') #1.5 # [config] self.config
        self.results = results
        
        
        
    
    def processWeeks(self): #processPeriods/seasons/ ...Call this 'processPeriods(self):
        """
        Here we set up the simulation horizon obtaining the demands on each busbar across the network 
        """
        
        # Before anything - get the base demands and send to baseCase()
        self.baseCase()
        self.baseV = self.nomV
        self.baseTap = self.tapPC 
        
        
        periods = (self.config.get('simulation', 'periods'))
        periods = periods.translate(None, "()''")           #periods = re.sub('[()]', '', periods)
        #datetime.datetime.strptime(a[0], '%H:%M')
        periods = periods.split(',')
        for x in periods:
            print x
        
        t = periods[0] # we're only doing one period...
        powerKeys = self.datasource.PowerList(t) # instance of the demand at a given time - dictionary or list?
        
#        with LoadFLowModel(self.config) as simulation:
        self.insertValues(powerKeys,self.scaleFact) # not for just demand but transformer positions etc
                
        exitcode = self._processRow()
        return 1
        
        

    def _processRow(self):
        """

        """
#        self.ipsasys, self.net = simulation.getNetRef() # Makes the handle from the simulation available outside this class -- self.ipsasys, self.net in 'networksimulation_py.
        exitcode = 0;
#        while exitcode < 1:
        
        #Ensure...
        
        if not self.doLoadFlow(): raise Exception('Load flow failed') # calls doLoadFlow method - self.net.DoLoadFlow()
        
        # what is the status of the system...SEVERAL PERFORMANCE PARAMETERS

        self.Logs = CollectNetStatus(self.results, self.net, self.config) 
        #resultlog1, NomVolts  = resultlog.getNetParams() # Is it worth passing nominal volts here..., maybe         
        resultlog, vLimits  = self.Logs.getNetParams() # send the parameters for busbars here....
        evalVolts = resultlog['total'] # could compute stability factor here...
        self.NominalV
        #while resultlog['total'] > 0: # Assumes we'll get rid of we'll get rid of all excursions...
        self.resultlog = resultlog
        print resultlog
        self.Logs = CollectNetStatus(self.results, self.net, self.config) 
        
        
        control = ControlAction(self.config, self.net, self.ipsasys) # get handle to control unit...
        #if resultlog['total'] > 0 or any([len(resultlog['underV']) ==0, len(resultlog['overV'])==0]):
        if any([self.resultlog['underV'] ==0, self.resultlog['overV']==0]):
            """ send to control actions for transformers """
            #overV or underV situation?
        resultlog, nomV = tapControlOperations()
        self.resultlog = resultlog
        print resultlog
        xxxx
                        
                        
                    
                    
# control scheme for tap position...
#            if resultlog['underV'] > 0:
#                nomV = self.tx.GetDValue(ipsa.IscTransformer.SpecVPU)
#                while nomV
#                if nomV+0.1 <= 1.06:
#                    self.tx.SetDValue(ipsa.IscTransformer.SpecVPU, nomV+0.1) # +6% 
#                else:
#                    break
#                
#            elif resultlog['overV'] > 0:
#                nomV = self.tx.GetDValue(ipsa.IscTransformer.SpecVPU)
#                if nomV-0.1 >= 0.94:
#                    self.tx.SetDValue(ipsa.IscTransformer.SpecVPU, nomV-0.1) # +6% 
#                else:
#                    break
            
#            control = ControlAction(self.config, self.net, self.ipsasys) # get handle to control unit...
#            # is it under voltage or over voltage that the issue?
#            #control = ControlAction(self.config, self.net, self.ipsasys)
#            tapPC, baseNominalV = control.tapcontrol(resultlog) 
#            
#            #check - undervoltage is set to largest value
#            
#            if not self.doLoadFlow(): raise Exception('Load flow failed') # calls doLoadFlow method - self.net.DoLoadFlow()
#
#            Logs = CollectNetStatus(self.results, self.net, self.config) 
#            resultlog, vLimits  = Logs.getNetParams() # send the parameters for busbars here....
#                # is there a tolerance on the limits? set at 1% - put this in the control section...
#            
#            # What position were we in? overV or underV
#            print resultlog
#            xxx
#            # PICK UP HERE..
#            if resultlog['total'] != 0: # we need to do some stuff...
#                """ Do something """    
#            
#                print resultlog
#                xxx
#                
#            
#            if (resultlog['total'] - evalVolts) < 0:
#                """ Do something """ 
#                
#            elif (evalVolts - resultlog['total']) > 0:
#                """ Do something """ 
#                
#                # net worsening of the position
#                #0. Perform base case load flow with each bus 'at' capacity...
#                # 1. reset tap and enact DSR
#                # 2. what scheme to employ...5% reduction on each tap changer? Which bus is overloaded? does it run in the basecase (unity)
#                # 3. need to recal results so as to make a judgement on which tap changers to solve (put an idea of how they would be solved
#                #...understanding ADMD)  
#                
#            # take it back to the basecase and solve for that load situation...
#        
#        
#        
#                
#                
#                #resultlog, vLimits  = Logs.getNetParams() # send the parameters for busbars here....
#                
#            
#            if not self.doLoadFlow(): raise Exception('Load flow failed') # calls doLoadFlow method - self.net.DoLoadFlow()
#            resultlog = CollectNetStatus(self.results, self.net, self.config) 
#                
#            resultlog1, a = resultlog.getNetParams() # send the parameters for busbars here....
#            exitcode = resultlog # do we return a 'good to go' exit code...?
#            pass
#            #xxxxx
#            
#            # Document...
#            # Need to know how to solve the issues - rules based
#            # Overvoltage issue - move tap up from its current position
#            # Call controlactions when understanding what action to take
#            # Could pass a sequence of events to test...
#            # Switch, case statements - 
#            # Evaluate results here...or in controloptions class
#        
#        # The combination of actions (control) reduced the problems from x to x - important to capture control actions to update learning
    

        
    def controlSequence():
        """ figure control order to solve network problems - calls controlAction to determine limits"""
        
        # series of case statements here
        
    # adjust tap postion, enact DSR, source or sink real power to an energy storage device 
    
    # link with other actors in the system to obtain possible constraints 
        # achieved by a matrix of options
        # Assume we come back from here and altering the tap changer is the answer
        
        # ControlAction() # don't come out of this control unless solved - once all excursions soved then write to file
    
        # self.controlSeq = [] # could be a dictionary - what would be needed to get the network back to limits
        
        # Pick UP HERE
    
    
    def controlOptions(): # Not necessarily required - depends on future modelling arcitecture for EPO
        
        """ method that links with other actors to determine feasible bounds """
    
        # call to ESP
        # send/call to DSR
        
    def tapControlOperations(self):
        
        
        if any(value == 0 for value in self.resultlog.values()) == True: # suggests and under or over voltage issue...
            if self.resultlog['underV'] > 0:
                #underTapPC()
                print(self.resultlog['underV'])
                Vcond = self.resultlog['underV']
                for V in xrange(100,107):
                    
                    nomVv = V/100
                    print ('Entered underV loop', V/100)
                    tapPC, NominalV = self.control.tapsetting(V/100)
                    self.tapPC = tapPC; self.nomV = NominalV
                    if not self.doLoadFlow(): raise Exception('Load flow failed')
                    resultlog, vLimits  = self.Logs.getNetParams() # send the parameters for busbars here.... 
                    self.resultlog = resultlog
                    if self.resultlog['underV'] ==0:
                        exitcode = 0
                        print ('no more under volts')
                        return exitcode
                        #print ('no more under volts')
                        #break;
                    elif self.resultlog['underV'] > Vcond:
                        print ('its getting worse')
                        exitflag = -1
                        return exitflag
                        #break;
                        
            if self.resultlog['overV'] > 0:
            #overTapPC()
                Vcond = self.resultlog['overV']
                for V in xrange(94,101):
                    print ('Entered overV loop')
                    nomVv = V/100
                    tapPC, NominalV = self.control.tapsetting(V/100)
                    self.tapPC = tapPC; self.nomV = NominalV
                    if not self.doLoadFlow(): raise Exception('Load flow failed')
                    resultlog, vLimits  = self.Logs.getNetParams() # send the parameters for busbars here....  
                    self.resultlog = resultlog
                    if self.resultlog['overV'] ==0:
                        exitcode = 0
                        return exitcode
                        print ('no more over volts')
                        break;
                    elif self.resultlog['overV'] > Vcond:
                        print ('its getting worse')
                        break;

    
    def baseCase(self):
        """ Determine base voltage level """
        
        # get loads
        scaleFact = 0
        t = 'BaseCase'
        powerKeys = self.datasource.PowerList(t) # instance of the demand at a given time - dictionary or list?
        self.insertValues(powerKeys,scaleFact) # not for just demand but transformer positions etc
        #if any(value == 0 for value in self.resultlog.values()) == True:
        exitcode = 1
        i = 0
        while exitcode > 0:
            
            if i == 0:
                self.control = ControlAction(self.config, self.net, self.ipsasys) # get handle to control unit...
                #Ensure starting positions are set at unity...Tap locked at zero position with unity power factor setting..
                nomV = 1
                tapPC, baseNominalV = self.control.basetapsetting(nomV)
                if not self.doLoadFlow(): raise Exception('Load flow failed')
                self.Logs = CollectNetStatus(self.results, self.net, self.config) 
                resultlog, vLimits  = self.Logs.getNetParams() # send the parameters for busbars here....            
                self.resultlog = resultlog
                if self.resultlog['total'] == 0:
                    exitcode = 0
            else: # if voltage cannot be resolved then resort back to the 
                print ('entered Else statement for Base computation')
                #tapPC, baseNominalV = control.tapsetting(nomV) 
                exitcode = self.tapControlOperations()
                self.baseV = self.nomV
                self.baseTap = self.tapPC         
            
            if exitcode == -1:
                xxxxxx
            
            #if flag exist, artificailly break the code - then set exitcode = 0 
            #exitcode = 0 
            i+=1
        print (i,tapPC,baseNominalV)
        xxxx